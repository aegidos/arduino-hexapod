    #include <SoftwareSerial.h>
    #define TX_PIN 7
    #define RX_PIN 6
    #define RST_SSC 3

    #define SET_PULSE_WIDTH 0x4




    SoftwareSerial s=SoftwareSerial(RX_PIN, TX_PIN);


    /* Pololu Protocol:
    BYTE 0:  HEADER (always 0x80)
    BYTE 1:  Command to send
    BYTE 2:  Target Servo
    BYTE 3:  First data byte
    BYTE 4:  Second date byte. (If necessary)

    Data must be sent in LSB.
    SoftwareSerial::print() does this via:
    for (mask = 0x01; mask; mask <<= 1) { ... }
    */
    void set_servo_num(uint8_t number)
    {
      s.print( 0x80, BYTE );
      s.print( 0x02, BYTE);
      s.print( number, BYTE );
    }

    void servo_cmd( uint8_t servo, uint8_t cmd, uint8_t data1, uint8_t data2)
    {

      s.print( 0x80, BYTE );
      s.print( 0x01, BYTE);
      s.print( cmd, BYTE );
      s.print( servo, BYTE );
      s.print( data1, BYTE  );
      s.print( data2, BYTE  );

    }

    void servo_angle( int servo, int angle ) {
      //Convert the angle data into two 7-bit bytes
      unsigned int temp;
      unsigned char pos_hi,pos_low;
      temp=angle&0x1f80;
      pos_hi=temp>>7;
      pos_low=angle & 0x7f;
     
      servo_cmd( servo, SET_PULSE_WIDTH, pos_hi, pos_low );
    }


    void setup() {

      pinMode(RX_PIN, INPUT);
      s.begin(9600);
      pinMode(TX_PIN, OUTPUT);
      digitalWrite( TX_PIN, HIGH);  //idle state, just to be sure



     
      delay(1000);
           //set_servo_num(0x00);

    }

    void loop() {

      while(1) {


        servo_angle( 0, 0 );

        digitalWrite(13, HIGH); //blink the LED
       
        delay(500);
       
        servo_angle( 0, 8000 );

        digitalWrite(13, LOW);   

        delay(500);
      }

    }
