   #include <ServoTimeTimer1.h>

    
    #include <SoftwareSerial.h>

    #define TX_PIN 5
    #define RX_PIN 6
    #define SERVO16_PIN 10
    #define SERVO17_PIN 9
    #define RST_SSC 3
    #define SERVOSPEED 20
    #define DELAY 500
    #define FEET	6
    #define AXLES	3
    #define SET_PULSE_WIDTH 0x4
    #define MINPULSE  1400
    #define MAXPULSE  4600
    #define UPPULSE    3800
    #define DWNPULSE    2000
    #define CENTERPULSE 3000

    SoftwareSerial s=SoftwareSerial(RX_PIN, TX_PIN);
    int moveServo;           // raw user input  
    int minPulse    = 1400;    //1200 would be possible
    int maxPulse    = 4600;    //4800 possible
    int upPulse     = 3800;
    int dwnPulse     = 2000;
    int centerPulse = 3000;
    ServoTimeTimer1 servo16;
    ServoTimeTimer1 servo17;
    int coxa[FEET]	 = {0,1,2,3,4,5};
    int femur[FEET]  = {6,7,8,9,10,11};
    int tibia[FEET]	 = {16,12,13,14,15,17};
    //coxa	//femur	//tibia
    int matrix[FEET][AXLES];
    int movement_fwd[FEET]={0,4,2,5,1,3};

    /* Pololu Protocol:
    BYTE 0:  HEADER (always 0x80)
    BYTE 1:  Command to send
    BYTE 2:  Target Servo
    BYTE 3:  First data byte
    BYTE 4:  Second date byte. (If necessary)

    Data must be sent in LSB.
    SoftwareSerial::print() does this via:
    for (mask = 0x01; mask; mask <<= 1) { ... }
    */
void servo_speed( uint8_t servo, uint8_t setspeed) 
{
  s.print( 0x80, BYTE );
  s.print( 0x01, BYTE);
  s.print( 0x1, BYTE ); 
  s.print( servo, BYTE ); 
  s.print( setspeed, BYTE  ); 
}
void foot_speed_init(int foot)
{
          servo_speed(matrix[foot][0],SERVOSPEED);  
          delay(DELAY);
          servo_speed(matrix[foot][1],SERVOSPEED);
          delay(DELAY);
          servo_speed(matrix[foot][2],SERVOSPEED-5);
          delay(DELAY);
}
void foot_init(int foot)
{
          delay(DELAY);
          delay(DELAY);
          delay(DELAY);
          if(foot == 0 || foot == 1 || foot == 2)
                    servo_angle( matrix[foot][0], upPulse );
          else
                   servo_angle( matrix[foot][0], dwnPulse );
          delay(DELAY);
                    servo_angle( matrix[foot][1], centerPulse );
          delay(DELAY);
                    servo_angle( matrix[foot][2], centerPulse );
          delay(DELAY);
  
}
void foot_fwd(int foot)
{
	  Serial.println("*****fwd*****");
	//femur
          servo_angle( matrix[foot][0], upPulse );
          delay(500);
  	//tibia -->control if -14 or -15
          servo_angle( matrix[foot][1], dwnPulse );
          delay(500);
		//coxa
          servo_angle( matrix[foot][2], upPulse );
          delay(500);
		//tibia -->control if -14 or -15
          servo_angle( matrix[foot][1], centerPulse );
          delay(500);
          //femur
          servo_angle( matrix[foot][0], centerPulse );
          delay(500);
          //coxa
          servo_angle( matrix[foot][2], centerPulse );
          delay(500);
}

void foot_inv(int foot)
{
	  Serial.println("*****bck*****");
	//femur
          servo_angle( matrix[foot][0], dwnPulse );
          delay(500);
  	//tibia -->control if -14 or -15
          servo_angle( matrix[foot][1], upPulse );
          delay(500);
		//coxa
          servo_angle( matrix[foot][2], dwnPulse );
          delay(500);
		//tibia -->control if -14 or -15
          servo_angle( matrix[foot][1], centerPulse );
          delay(500);
          //femur
          servo_angle( matrix[foot][0], centerPulse );
          delay(500);
          //coxa
          servo_angle( matrix[foot][2], centerPulse );
          delay(500);
}

void init_matrix()
{
//***************<filling matrix>*************		
		uint8_t i,j =0;	
		for(i = 0; i < FEET; i++  )	
		{
				for(j = 0; j < AXLES; j++  )	
				{
					switch(j)
					{
						case 0:	
									matrix[i][j]=femur[i];
									break;
						case 1:
									matrix[i][j]=tibia[i];
									break;
						case 2:
									matrix[i][j]=coxa[i];
									break;
					}
				}
		}

//***************</filling matrix>*************	
}
void move_forward()
{
  uint8_t i;
		Serial.println("*************walking fwd***************\n");	
		for(i = 0; i < FEET; i++  )	
		{
			if(((i+1)%2)==0)
				{
				foot_inv(movement_fwd[i]);
			  }
			else
				{
				foot_fwd(movement_fwd[i]);	
				}
		}
}
void blink_LED_delay()
    {
        digitalWrite(13, HIGH); //blink the LED
       
        delay(500);
       

        digitalWrite(13, LOW);   

        delay(500);
    }
void coxa_center()
    {
              servo_angle( 8, centerPulse );
              delay(DELAY);
              servo_angle( 9, centerPulse );
              delay(DELAY);
              servo_angle( 10, centerPulse );
              delay(DELAY);
              servo_angle( 11, centerPulse );
              delay(DELAY);
              servo_angle( 12, centerPulse );
              delay(DELAY);
              servo_angle( 13, centerPulse );
              delay(DELAY);
    }

void coxa_fwd()
   {
              servo_angle( 8, maxPulse );
              delay(DELAY);
              servo_angle( 9, maxPulse );
              delay(DELAY);
              servo_angle( 10, maxPulse );
              delay(DELAY);
              servo_angle( 11, minPulse );
              delay(DELAY);
              servo_angle( 12, minPulse );
              delay(DELAY);
              servo_angle( 13, minPulse );    
              delay(DELAY);   
   }
void coxa_bck()
  {
              servo_angle( 8, minPulse );
              delay(DELAY);
              servo_angle( 9, minPulse );
              delay(DELAY);
              servo_angle( 10, minPulse );
              delay(DELAY);
              servo_angle( 11, maxPulse );
              delay(DELAY);
              servo_angle( 12, maxPulse );
              delay(DELAY);
              servo_angle( 13, maxPulse );
              delay(DELAY);
  }
void femur_center()
    {
              servo_angle( 14, centerPulse );
              delay(DELAY);
              servo_angle( 15, centerPulse );
              delay(DELAY);
              servo_angle( 0, centerPulse );
              delay(DELAY);
              servo_angle( 1, centerPulse);
              delay(DELAY);
              servo_angle( 2, centerPulse );
              delay(DELAY);
              servo_angle( 3, centerPulse );       
              delay(DELAY);
    }
void femur_fwd()
  {
    uint8_t i = 0;
    for(i = 0; i < (FEET); i++  )
    {
      servo_angle( femur[i], centerPulse);
    }
  }
void femur_bck()
  {
    uint8_t i = 0;
    for(i = 0; i < (FEET/2); i++  )
    {
      servo_angle( femur[i], upPulse);
    }
    for(i = (FEET/2); i < FEET; i++  )
    {
      servo_angle( femur[i], dwnPulse);
    }
  }
void tibia_center()
    {
      int pos;
      pos=500;
            servo16.write(pos);
      delay(DELAY);
            servo_angle( tibia[1], centerPulse);
      delay(DELAY);
            servo_angle( tibia[2], centerPulse);
      delay(DELAY);
            servo_angle( tibia[3], centerPulse);
      delay(DELAY);
            servo_angle( tibia[4], centerPulse);
      delay(DELAY);
       pos=2000;
            servo17.write(pos);
    }
void tibia_up()
    {
          int pos;
         pos=2000;
            servo16.write(pos);
      delay(DELAY);
            servo_angle( tibia[1], upPulse);
      delay(DELAY);
            servo_angle( tibia[2], upPulse);
      delay(DELAY);
            servo_angle( tibia[3], dwnPulse);
      delay(DELAY);
            servo_angle( tibia[4], dwnPulse);
      delay(DELAY);
      pos=500;
            servo17.write(pos);
    }
void tibia_bck()
    {
              servo16.write(180);
              delay(DELAY);
              servo_angle( 4, maxPulse );
              delay(DELAY);
              servo_angle( 5, maxPulse ); 
              delay(DELAY);
              servo_angle( 6, minPulse );
              delay(DELAY);
              servo_angle( 7, minPulse );
              delay(DELAY);
              servo17.write(0);          
              delay(DELAY);
    }    
    
void servo_cmd( uint8_t servo, uint8_t cmd, uint8_t data1, uint8_t data2)
    {

      s.print( 0x80, BYTE );
      s.print( 0x01, BYTE);
      s.print( cmd, BYTE );
      s.print( servo, BYTE );
      s.print( data1, BYTE  );
      s.print( data2, BYTE  );

    }

    void set_servo_num(uint8_t number)
    {
      s.print( 0x80, BYTE );
      s.print( 0x02, BYTE);
      s.print( number, BYTE );
    }

    void servo_angle( int servo, int angle ) {
          //Convert the angle data into two 7-bit bytes
       int tmpAngle;
      Serial.println(servo);
      if(servo < 16)
      {
      //Convert the angle data into two 7-bit bytes
      unsigned int temp;
      unsigned char pos_hi,pos_low;
      temp=angle&0x1f80;
      pos_hi=temp>>7;
      pos_low=angle & 0x7f;
      servo_cmd( servo, SET_PULSE_WIDTH, pos_hi, pos_low );
      }
      else
      {

          switch (angle)
          {
            case MINPULSE:
              tmpAngle = 10;
              break;
            case MAXPULSE:
              tmpAngle = 170;
              break;
            case UPPULSE:
              tmpAngle = 150;
              break;
            case DWNPULSE:
              tmpAngle = 25;
              break;
            case CENTERPULSE:
              tmpAngle = 90;            
              break;
          }
     if(servo == 16)
        {
         servo16.write(tmpAngle);
          Serial.println(tmpAngle);
        }
        else
        {
        servo17.write(tmpAngle);
        Serial.println(tmpAngle);
        }
      }
    }


    void setup() {

      pinMode(RX_PIN, INPUT);
      s.begin(9600);
      pinMode(TX_PIN, OUTPUT);
      pinMode(9, OUTPUT);
      pinMode(10, OUTPUT);
      digitalWrite( TX_PIN, HIGH);  //idle state, just to be sure
      servo16.attach(SERVO16_PIN);
      //servo16.setMinimumPulseWidth(2000);
      //servo16.setMaximumPulseWidth(4000);
      
      servo17.attach(SERVO17_PIN);


     
      delay(1000);
      Serial.begin(9600);  
      Serial.println("Arduino Serial Servo Control");
      Serial.println("Press < or > to move coxa, spacebar to center coxa");  
      Serial.println("Press 1 to center femur :: 2 femur forward :: 3 to femur backward");        
      Serial.println("Press 4 to  bck tibia :: 5 center tibia :: 6 to SET SPEED");              
      Serial.println("Press 7 to initialize :: 8 to run");              
      Serial.println();  
      init_matrix();
      //set_servo_num(0x00);
     Serial.print("6 --> SET SPEED");
     foot_speed_init(0);
     foot_speed_init(1);
     foot_speed_init(5);
     foot_speed_init(2);
     foot_speed_init(3);
     foot_speed_init(4);
     Serial.println("6 -->READY SET SPEED");      
    }

    void loop() {

      while(1) {
      if (Serial.available() > 0) {
        moveServo = Serial.read();  

        // ASCII �>' is 62, ASCII '<� is 60 (comma and period, really)  
        if (moveServo == 60) {
          Serial.println("< coxa bckwrd");  
          //coxa_bck();
        }  
        if (moveServo == 62) {
          Serial.println("> coxa fwd");
          //coxa_fwd();
        }  
        if (moveServo == 32) { 
              Serial.println("spacebar center coxa");
            //  coxa_center();
        } 
        if (moveServo == 49) {
              Serial.print("1 --> center femur");
              //femur_center();    
        }
        if (moveServo == 50) {
              Serial.print("2 --> fwd femur");
              //Beinchen runter
              femur_fwd();    
        }
        if (moveServo == 51) {
              Serial.print("3 --> bck femur");
              //Beinchen hoch
              femur_bck();    
        }
        if (moveServo == 52) {
              Serial.print("4 -->  tibia UP");
                               
                 servo_angle( matrix[0][1], dwnPulse );
                 servo_angle( matrix[4][1], upPulse );
                 servo_angle( matrix[2][1], dwnPulse );

  
        }          
        if (moveServo == 53) {
              Serial.print("5 --> center tibia");
              tibia_center();    
        }    
        if (moveServo == 54) {
              Serial.print("6 --> COXA fwd");
            
                 servo_angle( matrix[0][2], upPulse );
                 servo_angle( matrix[4][2], dwnPulse );
                 servo_angle( matrix[2][2], upPulse );


        }   
        if (moveServo == 55){
              Serial.println("7 --> init feet");
              foot_init(0);
              foot_init(1);
              foot_init(2);
              foot_init(3);
              foot_init(4);
              foot_init(5);
              //End F��chen ausrichten ;-) steiler angriffswinkel f�r elevation
              tibia_up();  
              Serial.println("feet initialized");              
        }
        if (moveServo == 56){
              Serial.println("8 --> move foot all");
              delay(500);
                  //matrix[x][y] -->matrix[FOOT][ELEMENT] // FEET --> 0 to 5
                  // ELEMENT --> FEMUR | TIBIA | COXA
                  //rightside of MIA (feet 3,4,5) inverted direction !!!! 
                  
                  //FEMUR movement ---> Move them UP
//Movement Foot 4                  
                 servo_angle( matrix[4][0], dwnPulse ); // FEMUR UP DOWN
                 servo_angle( matrix[4][1], dwnPulse ); // TIBIA Tentacles UP DOWN
                 servo_angle( matrix[4][2], dwnPulse ); // COXA Forward Backward
                 delay(500);
                 servo_angle( matrix[4][0], centerPulse ); // FEMUR UP DOWN
                 
//Movement Foot 0              
                 servo_angle( matrix[0][0], upPulse ); // FEMUR UP DOWN
                 servo_angle( matrix[0][1], upPulse ); // TIBIA Tentacles UP DOWN
                 servo_angle( matrix[0][2], upPulse ); // COXA Forward Backward
                 delay(500);
                 servo_angle( matrix[0][0], centerPulse ); // FEMUR UP DOWN
                 
//Movement Foot 2                 
                 servo_angle( matrix[2][0], upPulse ); // FEMUR UP DOWN
                 servo_angle( matrix[2][1], upPulse ); // TIBIA Tentacles UP DOWN
                 servo_angle( matrix[2][2], upPulse ); // COXA Forward Backward
                 delay(500);
                 servo_angle( matrix[2][0], centerPulse ); // FEMUR UP DOWN

//FORWARD FEET 4,2,0
                 delay(500);
                 servo_angle( matrix[0][2], centerPulse ); // COXA Forward Backward
                 servo_angle( matrix[4][2], centerPulse ); // COXA Forward Backward
                 servo_angle( matrix[2][2], centerPulse ); // COXA Forward Backward                 
              
//Movement Foot 1
                 servo_angle( matrix[1][0], upPulse ); // FEMUR UP DOWN
                 servo_angle( matrix[1][1], upPulse ); // TIBIA Tentacles UP DOWN
                 servo_angle( matrix[1][2], upPulse ); // COXA Forward Backward
                 delay(500);
                 servo_angle( matrix[1][0], centerPulse ); // FEMUR UP DOWN
//Movement Foot 5
                 servo_angle( matrix[5][0], dwnPulse ); // FEMUR UP DOWN
                 servo_angle( matrix[5][1], dwnPulse ); // TIBIA Tentacles UP DOWN
                 servo_angle( matrix[5][2], dwnPulse ); // COXA Forward Backward
                 delay(500);
                 servo_angle( matrix[5][0], centerPulse ); // FEMUR UP DOWN
//Movement Foot 3
                 servo_angle( matrix[3][0], dwnPulse ); // FEMUR UP DOWN
                 servo_angle( matrix[3][1], dwnPulse ); // TIBIA Tentacles UP DOWN
                 servo_angle( matrix[3][2], dwnPulse ); // COXA Forward Backward
                 delay(500);
                 servo_angle( matrix[3][0], centerPulse ); // FEMUR UP DOWN

//FORWARD FEET 1,5,3
                 delay(500);
                 servo_angle( matrix[1][2], centerPulse ); // COXA Forward Backward
                 servo_angle( matrix[5][2], centerPulse ); // COXA Forward Backward
                 servo_angle( matrix[3][2], centerPulse ); // COXA Forward Backward                  
              
              Serial.println("foot 0 fwd");
        }
                if (moveServo == 57){
              Serial.println("9 --> 1,3,5");
              delay(500);

                 servo_angle( matrix[5][0], dwnPulse );
                 servo_angle( matrix[1][0], upPulse );
                 servo_angle( matrix[3][0], dwnPulse );
                 
        }

        }
      }

    }
